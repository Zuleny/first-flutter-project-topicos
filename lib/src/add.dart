import 'package:flutter/material.dart';

import 'package:first_flutter_project_topicos/src/http_client.dart';

class Add extends StatefulWidget {
  const Add({Key? key}) : super(key: key);

  @override
  _AddState createState() => _AddState();
}

class _AddState extends State<Add> {
  int _firstNumber = 0;
  int _secondNumber = 0;
  final HttpClient _httpClient = HttpClient();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Hello App"),
        ),
        body: SingleChildScrollView(
            child: Padding(
                padding: const EdgeInsets.all(30.0),
                child: Column(
                  children: [
                    TextField(
                      keyboardType: TextInputType.number,
                      onChanged: (value) {
                        _firstNumber = int.parse(value);
                      },
                      decoration:
                          const InputDecoration(labelText: "Ingrese Numero"),
                    ),
                    TextField(
                      keyboardType: TextInputType.number,
                      onChanged: (value) {
                        _secondNumber = int.parse(value);
                      },
                      decoration:
                          const InputDecoration(labelText: "Ingrese Numero"),
                    ),
                    ElevatedButton.icon(
                        onPressed: () async => await _httpRequest(),
                        icon: const Icon(Icons.add),
                        label: const Text("Sumar"))
                  ],
                ))));
  }

  Future<void> _httpRequest() async {
    Map response = await _httpClient.post(
        "/api/adder", {"number1": _firstNumber, "number2": _secondNumber});
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("Resultado"),
          actions: [
            TextButton(
              child: const Text('Ok',
                  style: TextStyle(
                      color: Colors.deepPurple, fontWeight: FontWeight.bold)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
          content: Text("${response['result']}"),
        );
      },
    );
  }
}
