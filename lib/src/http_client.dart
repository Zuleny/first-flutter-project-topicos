import 'package:dio/dio.dart';

class HttpClient {
  String _origin = "http://192.168.100.47:4000";

  ///Get Method for a Http Resquest
  Future<Map> get(String route) async {
    Response response;
    var dio = Dio();
    try {
      response = await dio.get(_origin + route);
      return response.data;
    } catch (e) {
      print("Error with GET Method \n$e");
      return {"error": "ERROR_WITH_GET_METHOD"};
    }
  }

  ///Post Method for a Http Resquest
  Future<Map> post(String route, dynamic data) async {
    Response response;
    var dio = Dio();
    try {
      response = await dio.post(_origin + route, data: data);
      return response.data;
    } catch (e) {
      print("Error with Post Method \n$e");
      return {"error": "ERROR_WITH_POST_METHOD"};
    }
  }
}
